# config {stack,buffer,label}
def get_features(config,sent_dict):
    features = []
    # TODO Improve Features
    if (len(config[0]) > 1): 
        top = config[0][-1] # Top of the stack
        top2 = config[0][-2] # Second element of the stack
        
        # Feature1: takes the top FORM element of the stack
        top_stk_token_feature = 'TOP_STK_FORM_'+str(sent_dict['FORM'][top].lower()) 
        features.append(top_stk_token_feature)

        # Feature2: takes the second FORM element of the stack
        top_stk_token_feature = 'TOP2_STK_FORM_'+str(sent_dict['FORM'][top2].lower()) 
        features.append(top_stk_token_feature)

        # Feature3: takes the top FORM element of the stack and the second FORM element of the stack combined
        top_stk_token_feature = 'TOP_STK_FORM1_'+str(sent_dict['FORM'][top].lower()) + str(sent_dict['FORM'][top2].lower()) 
        features.append(top_stk_token_feature) 

        # Features4: takes the POSTAG element of the stack 
        top_stk_token_feature = 'TOP_STK_POSTAG_'+str(sent_dict['POSTAG'][top].lower()) 
        features.append(top_stk_token_feature) 

        # Feature5: takes the top POSTAG element of the stack and the second POSTAG element of the stack combined
        top_stk_postag_feature = 'TOP_STK_POSTAG1_'+str(sent_dict['POSTAG'][top].lower())+str(sent_dict['POSTAG'][top2].lower())
        features.append(top_stk_postag_feature) 

        # Feature6: takes the top CPOSTAG element of the stack
        top_stk_token_feature = 'TOP_STK_CPOSTAG_'+str(sent_dict['CPOSTAG'][top].lower()) 
        features.append(top_stk_token_feature) 

        # Feature7: takes the second CPOSTAG element of the stack
        top_stk_token_feature = 'TOP2_STK_CPOSTAG_'+str(sent_dict['CPOSTAG'][top2].lower()) 
        features.append(top_stk_token_feature) 

        # Feature8: takes the first TEXT element of the stack
        top_stk_token_feature = 'TOP_STK_TEXT_'+str(sent_dict['TEXT'][top].lower()) 
        features.append(top_stk_token_feature)  

        # Feature9: takes the top TEXT element of the stack and the second TEXT element of the stack combined
        top_stk_token_feature = 'TOP_STK_TEXT1_'+str(sent_dict['TEXT'][top2].lower()) +str(sent_dict['TEXT'][top].lower()) 
        features.append(top_stk_token_feature)  
    #F1: 66.92   


    if (len(config[1]) > 0): 
        top = config[1][-1]     
        # Feature10: takes the first LEMMA element of the buffer
        top_stk_token_feature = 'TOP_BUFF_LEMMA_'+str(sent_dict['LEMMA'][top].lower()) 
        features.append(top_stk_token_feature) 

        # Feature11: takes the first TEXT element of the buffer
        top_stk_token_feature = 'TOP_BUFF_TEXT_'+str(sent_dict['TEXT'][top].lower()) 
        features.append(top_stk_token_feature) 

        # Feature12: takes the first POSTAG element of the buffer
        top_stk_token_feature = 'TOP_BUFF_POSTAG_'+str(sent_dict['POSTAG'][top].lower()) 
        features.append(top_stk_token_feature)
    # F1: 93.87


    if (len(config[1]) > 1): 
        top = config[1][-1] # First element in the buffer
        top2 = config[1][-2] # Second element in the buffer

        # Feature13: takes the second POSTAG element of the buffer
        top_stk_token_feature = 'TOP_BUFF_POSTAG2_'+str(sent_dict['POSTAG'][top2].lower()) 
        features.append(top_stk_token_feature)

        # Feature14: takes the top TEXT element of the buffer and the second TEXT element of the buffer combined
        top_stk_token_feature = 'TOP_BUFF_TEXT2_'+str(sent_dict['TEXT'][top].lower()) +str(sent_dict['TEXT'][top2].lower()) 
        features.append(top_stk_token_feature) 

        # Feature15: takes the second CPOSTAG element of the buffer
        top_stk_token_feature = 'TOP_BUFF_CPOSTAG2_'+str(sent_dict['CPOSTAG'][top2].lower())
        features.append(top_stk_token_feature) 
    # F1: 93.89


    return features
