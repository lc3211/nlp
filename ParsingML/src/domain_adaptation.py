from dataset import load_data
from dependency import *
from feature_extraction import *
from feature_extraction_da import *
from sklearn.svm import *
from utils import *
import numpy as np
import pickle


#This function will train the model using LinearSVC function. 
#Input: training data
#Output: train model
def train_model_da(data):
    X,y = get_feature_vectors_for_training_da(data)
    print("Number of training samples : " + str(y.shape[0]))
    print("Number of features : "+str(X.shape[1]))
    print("Training model")
    model = LinearSVC()
    model.fit(X,y)
    print("Completed")
    return model

#This function will use the features to be included in the training model
#Input: data 
#Output: will return the data with labels to be included in the training model
def get_feature_vectors_for_training_da(data):
    projective_tree_count = 0
    projective_non_parsable = []
    X = []
    y = []
    for id,sent_dict in data.items():
        if len(sent_dict['FORM']) == 1:
           #Example : train file, line 97384. Text : '************************'
            continue
        gold_arcs = get_gold_arcs(sent_dict['HEAD'])
        buff = [i for i in range(len(sent_dict['FORM']))[::-1]]
        projective = is_projective(gold_arcs, len(sent_dict['FORM']))

        if not projective:
            continue
        try:
            dgraph, configurations = make_transitions(buff, oracle_std, gold_arcs)
        except IndexError:
            projective_non_parsable.append(sent_dict)
            continue

        for config in configurations:
            X.append(get_features_da(config[:2],sent_dict))
            y.append(config[2])
        # Root missing.
        if set(gold_arcs)-set(dgraph):
            print("Missing arcs",set(gold_arcs)-set(dgraph))
        projective_tree_count+=1

    feature_values = set([feature for row in X for feature in row])
    feature_dict = {feature: i for i, feature in enumerate(feature_values)}
    with open('feature_dict_da.pkl', 'wb') as f:
        pickle.dump(feature_dict,f)

    X_ = one_hot_encoding(X,feature_dict)
    a = 0.0
    for i in range(len(X_)):
        b = float(sum(X_[i]))
        c = float(len(X[i]))
        a += b
    y = np.array(y)

    print("Number of valid projection trees : "+str(projective_tree_count))
    return X_,y

def predict_arcs(conll_dict,model,feature_dict):
    buff = [i for i in range(len(conll_dict['FORM']))[::-1]]
    stack, dgraph = [], []
    while (len(buff) > 0 or len(stack) > 1):
        config = (stack,buff,dgraph)
        features = get_features(config,conll_dict)
        binary_features = one_hot_encoding([features],feature_dict)
        choice = model.predict(binary_features)
        try:
            if choice == 'shift':   shift(stack, buff, stack)
            elif choice == 'left_arc': left_arc(stack, buff, dgraph)
            elif choice == 'right_arc': right_arc(stack, buff, dgraph)
            else: return None
        except IndexError:
            break
    return dgraph

def print_metrics(predictions, gold_arcs):
    precision = []
    recall = []
    for prediction,gold_arc in zip(predictions, gold_arcs):
        correct_arcs = set(gold_arc) & set(prediction)
        if not prediction:
            precision.append(0)
        else:
            precision.append(float(len(correct_arcs))/len(prediction))
        recall.append(float(len(correct_arcs)) / len(gold_arc))
    total_precision = float(sum(precision))/float(len(precision))
    total_recall = float(sum(recall))/float(len(recall))
    f1 = 2*float(total_precision*total_recall)/float(total_precision+total_recall)

    print("Total precision :"+str(total_precision))
    print("Total recall: "+str(total_recall))
    print('F1 Measure: '+str(f1))

if __name__ == '__main__':
    train_model('en-ud-train.conllu')
    print("Training complete")
    test_model('en-ud-dev.conllu')


