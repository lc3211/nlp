# config {stack,buffer,label}
def get_features_da(config,sent_dict):
    features = []
    # TODO Improve Features

    if (len(config[0]) > 1): 
        # Top of stack.
        top = config[0][-1] #Top of the stack (1st element)
        top2 = config[0][-2] #Second elemenet of the stack
        
        # Feature1: This will take the FORM of top of the stack
        top_stk_token_feature = 'TOP_STK_FORM_'+str(sent_dict['FORM'][top].lower()) 
        features.append(top_stk_token_feature)  

        # Feature2: This will take the POSTAG of the top of the stack
        top_stk_token_feature = 'TOP_STK_POSTAG_'+str(sent_dict['POSTAG'][top].lower()) 
        features.append(top_stk_token_feature) 

        # Feature3: This will take the CPOSTAG of the top of the stack
        top_stk_token_feature = 'TOP_STK_CPOSTAG_'+str(sent_dict['CPOSTAG'][top].lower()) 
        features.append(top_stk_token_feature) 

        # Feature4: This will take the second element  CPOSTAG of the stack
        top_stk_token_feature = 'TOP2_STK_CPOSTAG_'+str(sent_dict['CPOSTAG'][top2].lower()) 
        features.append(top_stk_token_feature) 

        # Feature5: This will take the second element TEXT of the stack
        top_stk_token_feature = 'TOP2_STK_TEXT_'+str(sent_dict['TEXT'][top2].lower()) 
        features.append(top_stk_token_feature) 

        # Feature6: This will take the second element  LEMMA of the stack
        top_stk_token_feature = 'TOP2_STK_LEMMA_'+str(sent_dict['LEMMA'][top2].lower()) 
        features.append(top_stk_token_feature) 

    
    if (len(config[1]) > 0): 
        top = config[1][-1] #takes first element of the buffer

        # Feature7: This will take the first TEXT element of the buffer 
        top_stk_token_feature = 'TOP_BUFF_TEXT_'+str(sent_dict['TEXT'][top].lower()) 
        features.append(top_stk_token_feature) 

        # Feature8: This will take the first POSTAG element of the buffer
        top_stk_token_feature = 'TOP_BUFF_POSTAG_'+str(sent_dict['POSTAG'][top].lower()) 
        features.append(top_stk_token_feature) 


    return features
