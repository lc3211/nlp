def shift(stack, buff,  dgraph): #push the next word in the buffer onto the stack
    # TODO
    stack.append(buff.pop()) # pops the last element in the buffer
    #print('shift')
    
def left_arc(stack, buff, dgraph): # add an arc from the topmost word on the stack, s1, to the second topmost word, s2, and pop s2.  
    # TODO
    #j is on the top of the stack, i is the 2nd item on the top. So you can think of it as 
    #you take i and j off the top and push j back on, You use both.
    j=stack.pop() 
    i=stack.pop()
    dgraph.append((i,j)) #(dependent, HEAD)
    stack.append(j)
    #print("left_arc")

def right_arc(stack, buff, dgraph):#add an arc from the second topmost word on the stack, s2, to the topmost word, s1, and pop s1
    # TODO
    j=stack.pop() 
    i=stack.pop()
    dgraph.append((j,i)) #(dependent, HEAD)
    stack.append(i)
    #print('right_arc')

def oracle_std(stack, buff, dgraph, gold_arcs): # Oracle: an oracle is a deterministic (or may be
# non-deterministic) algorithm that taking a gold
# tree associated to a sentence, it produces the
# set of actions the algorithm should follow in
# order to reach the gold tree.
    # TODO
    #print("stack:",stack)
    #print("buff:",buff)
    #print("Arc:",gold_arcs)
    #print("dgraph:",dgraph)
    if(len(stack)<=1):
        return('shift')
    elif((stack[-2], stack[-1]) in gold_arcs):
        return('left_arc')
    elif((stack[-1],stack[-2]) in gold_arcs):
        if(confirmsRight(gold_arcs, stack, dgraph)):
            return('right_arc')
        else:
            return('shift')
    else: 
        return('shift')

def confirmsRight(gold_arcs, stack, dgraph):
    set1 = []
    set2 = []
    for arc in gold_arcs:
        if(stack[-1] in arc[1:]):
            #print("gold",stack[-1])
            set1.append(arc)
            #print(set1)
    for arc in dgraph:
        if(stack[-1] in arc[1:]):
            #print("dgraph",stack[-1])
            set2.append(arc)
    return(all(map(lambda v: v in set2, set1)))


def make_transitions(buff, oracle, gold_arcs=None):
    stack = [] #containing the words of w
    dgraph = [] #will store the moves
    configurations = [] # a configuration for a sentence w=w1,...,wn concists of three components (buff, stack, dgraph)
    #buffer containing the words of w
    
    while (len(buff) > 0 or len(stack) > 1):
        choice = oracle(stack, buff, dgraph, gold_arcs)
        # Makes a copy. Else configuration has a reference to buff and stack.
        config_buff = list(buff)
        config_stack = list(stack)
        configurations.append([config_stack,config_buff,choice])
        if choice == 'shift':	shift(stack, buff, dgraph)
        elif choice == 'left_arc': left_arc(stack, buff, dgraph)
        elif choice == 'right_arc': right_arc(stack, buff, dgraph)
        else: return None
    return dgraph,configurations
