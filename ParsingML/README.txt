Name: Liliana Cruz-Lopez
Email: lc3211@columbia.edu
Project: Parsing & Machine Learning
Natural Language Processing



How to run the code you need to run the following commands:
  
python3 eval.py 
python3 domain_adaptation_eval.py 
python3 validate_transitions.py

Note: I made a slightly change in file domain_adaptation_eval.py and I commented eval.py in the head of the file. The reason is that my numbers were changing in part 2 every time I edited my features in part1. 

Part 1 - Arc Standard
1. Output of your validate_transitions.py 
FORMS : ['ROOT', 'It', 's', 'now', 'my', 'home', '.']
GOLD ARCS : [(1, 5), (2, 5), (3, 5), (4, 5), (5, 0), (6, 5)]
TRANSITIONS : 
shift
shift
shift
shift
shift
shift
left_arc
left_arc
left_arc
left_arc
shift
right_arc
right_arc

Values before feature selection.
Number of valid projection trees : 1913
Number of training samples : 11949
Number of features : 1811
Training model
Completed
Saving model
Training complete
Total precision :0.5070533227218463
Total recall: 0.23754034575761596
F1 Measure: 0.32352040232321044

2. List all the feature types you implemented. [Ex: ‘TOP_STK_TOKEN_’ , ‘...’, ‘....’]
For our reference.
top = first item in the stack/buffer
top2 = second item in the stack/buffer

'TOP_STK_FORM_'+str(sent_dict['FORM'][top].lower()) 
'TOP2_STK_FORM_'+str(sent_dict['FORM'][top2].lower()) 
'TOP_STK_FORM1_'+str(sent_dict['FORM'][top].lower()) + str(sent_dict['FORM'][top2].lower()) 
'TOP_STK_POSTAG_'+str(sent_dict['POSTAG'][top].lower()) 
'TOP_STK_POSTAG1_'+str(sent_dict['POSTAG'][top].lower())+str(sent_dict['POSTAG'][top2].lower())
'TOP_STK_CPOSTAG_'+str(sent_dict['CPOSTAG'][top].lower()) 
'TOP2_STK_CPOSTAG_'+str(sent_dict['CPOSTAG'][top2].lower())
'TOP_STK_TEXT_'+str(sent_dict['TEXT'][top].lower()) 
'TOP_STK_TEXT1_'+str(sent_dict['TEXT'][top2].lower()) +str(sent_dict['TEXT'][top].lower())
'TOP_BUFF_LEMMA_'+str(sent_dict['LEMMA'][top].lower()) 
'TOP_BUFF_TEXT_'+str(sent_dict['TEXT'][top].lower()) 
'TOP_BUFF_POSTAG_'+str(sent_dict['POSTAG'][top].lower()) 
'TOP_BUFF_POSTAG2_'+str(sent_dict['POSTAG'][top2].lower()) 
'TOP_BUFF_TEXT2_'+str(sent_dict['TEXT'][top].lower()) +str(sent_dict['TEXT'][top2].lower()) 
'TOP_BUFF_CPOSTAG2_'+str(sent_dict['CPOSTAG'][top2].lower())

3. Explain any 6 feature types in detail. What does the feature type represent? Why do you think this is useful for transition predictions?
'TOP_BUFF_LEMMA_' This feature aims to remove inflectional endings only and to return the base or dictionary form of a word from the buffer. For example, 
# text = I realize they are young and that cats do climb, but this is just ridiculous.
# text = But to stand, day after day, and to make such preposterous statements, known to everybody to be lies, without even being ridiculed in your own milieu, can only happen in this region. 

'TOP_BUFF_POSTAG_' This feature helps us to know what part of speech (POS) or word class (noun,verb,…) should be assigned to words in a piece of text. This will help the classifier by tagging the words. It will learn which tags are linked together and therefore more probable being linked in future. 

'TOP_STK_POSTAG_' This feature help is similar to the one above but here we will know which tags will be paired. For example, for the sentence “American forces killed Shaikh Abdullah al-Ani, the preacher at the mosque in the town of Qaim, near the Syrian border.” We have the following output.
JJ NNS VBD NNP NNP NNP , , DT NN IN DT NN IN DT NN IN NNP , , IN DT JJ NN . .

'TOP_STK_FORM_'  -This will help us to obtain the form of the top of the stack. With this we get:
TOP_STK_FORM_executed 	
TOP_STK_FORM_imprisoned
TOP_STK_FORM_debra
TOP_STK_FORM_perlingiere
This can help us to see the words that can come up more frequently and how they can depend from each other.
 
'TOP2_STK_FORM_' This feature, with the one above, are really strong combined. This help us to get the form of the first and second item in the stack. 

‘TOP2_STK_CPOSTAG’ This feature collaborates better with POSTAG since it is a more compact and has lets options for the tags that works as follow. You can notice that it focusses more on the ones that are most important in the sentence. However, if it is implemented by itself then it does not work properly or benefit the classifier. 
TOP2_STK_CPOSTAG_propn
TOP2_STK_CPOSTAG_root
TOP2_STK_CPOSTAG_noun
TOP2_STK_CPOSTAG_root
TOP2_STK_CPOSTAG_root
TOP2_STK_CPOSTAG_intj
TOP2_STK_CPOSTAG_root
TOP2_STK_CPOSTAG_verb
TOP2_STK_CPOSTAG_root


4. How do you interpret precision and recall in context of this task?
Precision: % of selected items that are correct: TruePositive/ (TruePositive+FalsePositive)
Recall: % of correct items that are selected: TruePositive/ (TruePositive+FalseNegative)
In this assignment, we can see that both of the numbers are the same because the classifier is able to identify TruePositives and TrueNegatives with the same accuracy. However, if recall goes down then precision will go also down since there is a tradeoff. That is one of the reasons we use F1 in order to compare classifiers instead of focusing in only precision or only recall. In addition, the number of true arcs vs number of reported arcs, for each sentence, if valid parsed tree is produced, there should be exactly one arc. Which means that the reported arcs and golden arcs produced a valid tree. True arcs= Reported arcs.


5. What is your final F1 score on the dev set?
Number of valid projection trees : 1913
Number of training samples : 11949
Number of features : 11187
Training model
Completed
Saving model
Training complete
Total precision :0.9389109961533361
Total recall: 0.9389109961533361
F1 Measure: 0.938910996153336

Part 2 - Domain Adaptation
6. Average F1 score from 10 runs of domain_adaptation_eval.py.
train_genre: review, test_genre: email, Avg F1: 0.8361337730934690
train_genre: review, test_genre: newsgroup, Avg F1: 0.8840579710144920


7. Provide an explanation of the performance of the feature types in domain adaptation. What features generalized well?
For part 2 I used only 8 features. In the beginning, I started by adding features one by one and recorded the change. However, after sometime I noticed that if I added features more that I in the beginning did not work such as LEMMA it increased my F1 score. Therefore, I deduced that these features work well together as a group. The features below increased my F1 score by over 40 percent for emails and newsgroup. Also, not all the features from part 1 did well in part 2 because reviews information is not the same as emails and newsgroup. However, I included some of them. 

'TOP_BUFF_POSTAG_’
'TOP_BUFF_LEMMA_' 

 What features did not?
If I added any other function that included the second item in the buffer then my F1 would drop drastically. This might is because the second item in stack is not relevant or does not correlate with the other features.

